#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include<arpa/inet.h>
#include<string.h>

/* Programme serveur */

int main(int argc, char *argv[]) {

  /* Je passe en paramètre le numéro de port qui sera donné à la socket créée plus loin.*/

  /* Je teste le passage de parametres. Le nombre et la nature des
     paramètres sont à adapter en fonction des besoins. Sans ces
     paramètres, l'exécution doit être arrétée, autrement, elle
     aboutira à des erreurs.*/
  if (argc != 1){
    printf("utilisation : %s port_serveur\n", argv[0]);
    exit(1);
  }

  /* Etape 1 : créer une socket */
  int ds = socket(PF_INET, SOCK_DGRAM, 0);

  /* /!\ : Il est indispensable de tester les valeurs de retour de
     toutes les fonctions et agir en fonction des valeurs
     possibles. Voici un exemple */
  if (ds == -1){
    perror("Serveur : pb creation socket :");
    exit(1); // je choisis ici d'arrêter le programme car le reste
	     // dépendent de la réussite de la création de la socket.
  }

  /* J'ajoute des traces pour comprendre l'exécution et savoir
     localiser des éventuelles erreurs */
  printf("Serveur : creation de la socket réussie \n");

  // Je peux tester l'exécution de cette étape avant de passer à la
  // suite. Faire de même pour la suite : n'attendez pas de tout faire
  // avant de tester.

  /* Etape 2 : Nommer la socket du seveur */
  struct sockaddr_in sockServer;
  sockServer.sin_family = AF_INET;
  sockServer.sin_addr.s_addr = INADDR_ANY;
  sockServer.sin_port = htons(5000);
  if(bind(ds, (struct sockaddr *)&sockServer, sizeof(sockServer))){
      perror("Serveur : erreur au nommage : ");
      return 1;
  }

  //recuperer le numerro de port
  socklen_t lgA = sizeof(struct sockaddr_in);

  printf("Serveur : numero de port de la socket cree : %d", ntohs(sockServer.sin_port));
  /* Etape 4 : recevoir un message du client (voir sujet pour plus de détails)*/
  struct sockaddr_in sockClient;
  char m[1000];
  ssize_t res = recvfrom(ds, m, sizeof(m), 0, (struct sockaddr *)&sockClient, &lgA);
  if(res < 0){
      perror("Serveur : erreur a la reception ");
      exit(1);
  }

  printf("Serveur : message recu de la part de : %s:%d \n", inet_ntoa(sockClient.sin_addr), ntohs(sockClient.sin_port));
  printf("Serveur : nombre d'octet recu : %d \n", (int)res);
  printf("Serveur : message reçu : %s \n", m);

  /* Etape 5 : envoyer un message au serveur (voir sujet pour plus de détails)*/
  int r = sendto(ds, &res, sizeof(int), 0, (struct sockaddr *)&sockClient, lgA);
  if(r < 0)
  {
      perror("Serveur : erreur a l'envoi :");
      return 1;
  }

  printf("Serveur : reponse au client : %d \n", (int)res);


  /* Etape 6 : fermer la socket (lorsqu'elle n'est plus utilisée)*/
  close(ds);
  printf("Serveur : je termine\n");
  return 0;
}
