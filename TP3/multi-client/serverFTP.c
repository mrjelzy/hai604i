#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/stat.h>


int main(int argc,char * argv[]){

    if(argc!=2){
        printf("Usage : %s port\n",argv[0]);
        exit(1);
    }
    uint16_t port;
    sscanf(argv[1],"%hd",&port);

    int sock_server=socket(AF_INET,SOCK_STREAM,0);
    printf("socket creer \n");

    struct sockaddr_in server;
    server.sin_addr.s_addr=INADDR_ANY;
    server.sin_family=AF_INET;
    server.sin_port=htons(port);
    

    bind(sock_server,(const struct sockaddr *) &server,sizeof(server));
    printf("bind done\n");

    
    char nomFichier[4000];
    listen(sock_server,5);
    printf("listen \n");


    struct sockaddr_in client[10];
    socklen_t n = sizeof(&client);
    
    int parent = getpid();

    int i=0;
    int client_sock[10];
    while(1){
        client_sock[i]=accept(sock_server, (struct sockaddr *)&client[i],&n);
        printf("accept \n");
        fork();
        if(getpid()!=parent){

            printf("le fork demare la receeeeption du client %i avec le pid %i\n",n,getpid());

            char nomFichier[2000];
            recv(client_sock[i],&nomFichier,sizeof(nomFichier),0); //Recevoir le nom du fichier
            //printf("nom du fichier : %s \n",nomFichier);

            int taille=0;
            read(client_sock[i],&taille,sizeof(taille)); //Recevoir la taille du fichier
            //printf("taille : %i bytes\n",taille);

            char* fichier_avec_chemin;


            fichier_avec_chemin = malloc(strlen(nomFichier)+sizeof("reception/")); //creation du char avec le chemin vers le fichier
            strcpy(fichier_avec_chemin, "reception/");//on y met reception/
            strcat(fichier_avec_chemin, nomFichier);//on rajoute le nom du fichier final

            FILE* Fichier =fopen(fichier_avec_chemin, "w");

            char* data=malloc(taille);
            int e=0;
            while(e<taille){
                recv(client_sock[i],&data[e],sizeof(data[e]),0);
                if(&data[e]==NULL) break;
                fwrite(&data[e],1,1,Fichier);
                e++;
            }    

            //printf("dans le fichier %s il doit y avoir %s \n",nomFichier,data);
    
            fclose(Fichier);
            free(fichier_avec_chemin);
            close(client_sock[i]);


            return 1;
        }

        i++;

    }

    while(wait(0)!=-1);

    close(sock_server);
    printf("fin \n");

    return 0;
}