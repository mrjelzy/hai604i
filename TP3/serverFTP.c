#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>


int main(int argc,char * argv[]){

    if(argc!=2){
        printf("Usage : %s port\n",argv[0]);
        exit(1);
    }
    uint16_t port;
    sscanf(argv[1],"%hd",&port);

    int sock_server=socket(AF_INET,SOCK_STREAM,0);
    printf("socket creer \n");

    struct sockaddr_in server;
    server.sin_addr.s_addr=INADDR_ANY;
    server.sin_family=AF_INET;
    server.sin_port=htons(port);
    

    bind(sock_server,(const struct sockaddr *) &server,sizeof(server));
    printf("bind done\n");

    
    char nomFichier[4000];
    listen(sock_server,5);
    printf("listen \n");


    struct sockaddr_in client;
    socklen_t n = sizeof(client);
    int client_sock=accept(sock_server, (struct sockaddr *)&client,&n);
    printf("accept \n");

    recv(client_sock,&nomFichier,sizeof(nomFichier),0); //Recevoir le nom du fichier
    printf("nom du fichier : %s \n",nomFichier);
    
    int taille=0;
    read(client_sock,&taille,sizeof(taille));
    printf("taille : %i bytes\n",taille);

    char* fichier_avec_chemin;
    fichier_avec_chemin = malloc(strlen(nomFichier)+sizeof("reception/")); 
    strcpy(fichier_avec_chemin, "reception/");
    strcat(fichier_avec_chemin, nomFichier); 

    FILE* Fichier =fopen(fichier_avec_chemin, "w");

    char* data=malloc(taille);
    int i=0;
    while(i<taille){
        recv(client_sock,&data[i],sizeof(data[i]),0);
        if(&data[i]==NULL) break;
        fwrite(&data[i],1,1,Fichier);
        i++;
    }

    //printf("dans le fichier %s il doit y avoir %s \n",nomFichier,data);

    

    close(client_sock);
    close(sock_server);
    fclose(Fichier);
    free(fichier_avec_chemin);
    printf("fin \n");

    return 0;
}